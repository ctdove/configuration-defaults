# Enable Powerlevel10k instant prompt
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Plugins to load
plugins=(
  git
  zsh-autosuggestions
  dirhistory
)

source $ZSH/oh-my-zsh.sh

# Set langauge (just in case)
export LANG=en_US.UTF-8

# Source Powerlevel10k configuration file
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
