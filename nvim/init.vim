" Autoindentation
set autoindent

" Set tab to 2 spaces
set expandtab tabstop=2 shiftwidth=2

" Show line numbers
set number

" Show tabline
set showtabline=2

" Set colorscheme
colorscheme sonokai

" Set hard wrap at 80 characters
set tw=80
set colorcolumn=+1




" Plugins to install
call plug#begin() 

" Theme
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'

" Completion plugin
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()




" Lightline Configuration
let g:lightline = {
  \ 'colorscheme': 'wombat',
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ], [ 'readonly', 'filename', 'modified' ] ]
  \ },
  \ 'tabline': {
  \   'left': [ ['buffers'] ],
  \   'right': [ ['close'] ]
  \ },
  \ 'component_expand': {
  \   'buffers': 'lightline#bufferline#buffers'
  \ },
  \ 'component_type': {
  \   'buffers': 'tabsel'
  \ }
  \ }

" Keybinds
"
" F2 to save
inoremap <F2> <C-\><C-o>:w<CR>
noremap  <F2> :w<CR>

" Disable arrow keys
map <up>    <nop>
map <down>  <nop>
map <left>  <nop>
map <right> <nop>

imap <up>    <nop>
imap <down>  <nop>
imap <left>  <nop>
imap <right> <nop>

" Switch between buffers
nnoremap <leader>] :bnext<CR>
nnoremap <leader>[ :bprevious<CR>

" Open vim config file
nnoremap <leader>C :e $MYVIMRC<CR>

" Tab Completion/CoC Configuration
"
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
